var camera, scene, renderer;
var geometry, material, mesh, ref, pivotPoint;
var clock;

function init() {
  renderer = new THREE.WebGLRenderer();
  renderer.setClearColor(0x000000, 1);
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  scene = new THREE.Scene();

  geometry = new THREE.SphereGeometry(0, 0, 0);
  ref = new THREE.Mesh(geometry, material);
  ref.position.set(0, 0, 0);
  scene.add(ref);

  pivotPoint = new THREE.Object3D();
  pivotPoint.position.set(0, 0, 0);
  scene.add(pivotPoint);

  geometry = new THREE.BoxGeometry(0.5, 0.5, 0.5);
  material = new THREE.MeshBasicMaterial({ color: 0x156289, wireframe: false });
  mesh = new THREE.Mesh(geometry, material);
  pivotPoint.add(mesh);
  mesh.position.set(0, window.innerWidth / window.innerHeight, 0);
  camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    100
  );
  camera.position.set(0, 0, 3.3);
  camera.lookAt(pivotPoint.position);
    
  clock = new THREE.Clock();
  window.addEventListener("resize", onWindowResize, false);
}

var dir = 1;
function animate() {
  requestAnimationFrame(animate);

  var delta = clock.getDelta();

  pivotPoint.rotation.z += delta * 1;

  renderer.render(scene, camera);
}

function onWindowResize() {
  windowHalfX = window.innerWidth / 2;
  windowHalfY = window.innerHeight / 2;

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}
let animacion = function () {
  requestAnimationFrame(animacion);
  mesh.rotation.x = mesh.rotation.x + 0.08;
  mesh.rotation.y = mesh.rotation.y + 0.08;

  renderer.render(scene, camera);
};

window.addEventListener("DOMContentLoaded", function (event) {
  init();
  animate();
  animacion();
});
